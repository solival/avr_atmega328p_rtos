; RTOS Task and timer manager.
; Reserved registers REGISTERS:
; r14 (rtskpos) - current task offset in bytes (task offset * 2)
; r15 (rtsknxt) - next task offset in bytes (task offset * 2)
;
; Used resources:
; rtmph, rtmpl, rtmp2, Y, Z - temporary (safe)
.def rtskpos = r14
.def rtsknxt = r15
.def rtmph = r23
.def rtmpl = r24
.def rtmp2 = r25

.equ CORE_TIMER_LENGTH = 10	; Max reserved timers. Must be <= 63
.equ CORE_TASK_LENGTH = 10 ; Max number of tasks in queue. Must be <= 127

.dseg
coreTaskQueue:		.byte CORE_TASK_LENGTH*2	; Task queue size
coreTimerQueue:		.byte CORE_TIMER_LENGTH*4	; 2 byte task code, + 2 bytes of time offset

.cseg

; AddTask: macro wrapper to add core task
; RTOS
; @0 - task label
; Used resources:
; Z - temporary
.macro AddTask
				push ZL
				push ZH
				ldi ZL, low(@0)
				ldi ZH, high(@0)
				rcall AddCoreTask
				pop ZH
				pop ZL
.endmacro

; AddCoreTask: Add new task to execution (task addr in ZH:ZL)
; RTOS
; Used resources:
; Z - task address
; rtsknxt - offset for next task (shared, rw)
; rtskpos - current task offset (shared, ro)
; Y, rtmpl - temporary
AddCoreTask:
				push YL
				push YH
				push rtmpl

				ldi YL, low(coreTaskQueue)
				ldi YH, high(coreTaskQueue)
				clr rtmpl
				add YL, rtsknxt ; Set offset
				adc YH, rtmpl
				st Y+, ZH ; Write task code to coreTaskQueue+coreTaskNext
				st Y, ZL
				inc rtsknxt
				inc rtsknxt ; inc next task index on two bytes
				mov rtmpl, rtsknxt
				cpi rtmpl, CORE_TASK_LENGTH * 2 ; If coreTaskNext == CORE_TASK_LENGTH
				brne PACTSK01
				clr rtsknxt ; then coreTaskNext = 0 
PACTSK01:		cpse rtsknxt, rtskpos ; If coreTaskNext == coreTaskPos
				rjmp PACTSKEND ; then sendException 2
				rjmp CoreException
PACTSKEND:
				pop rtmpl
				pop YH
				pop YL
				ret

; RunTasks: Run all tasks in queue. 
; !Not safe for registers!
; RTOS
; Used resources:
; rtskpos - current task offset (shared, rw)
; rtsknxt - next task offset (shared, ro)
; Y, Z, rtmpl - temporary (NOT safe)
RunTasks:
				cpse rtskpos, rtsknxt	; if coreTaskPos == coreTaskNext
				rjmp RT01
				ret	; then return (no more tasks)
RT01:
				ldi YL, low(coreTaskQueue)	; Load address of queu to Y
				ldi YH, high(coreTaskQueue)
				clr rtmpl
				add YL, rtskpos ; prepare offset of task
				adc YH, rtmpl
				ld ZH, Y+ ; Load task address
				ld ZL, Y
;				 lsl ZL ; Multiply address on two (TODO: debug and check if needed)
;				 rol ZH
				icall	; exec task by address in register Z
				inc rtskpos ; Increase current task offset on two bytes
				inc rtskpos
				mov rtmpl, rtskpos
				cpi rtmpl, CORE_TASK_LENGTH * 2 ; if coreTaskPos == CORE_TASK_LENGTH
				brne RunTasks
				clr rtskpos	; then coreTaskPos = 0
				rjmp RunTasks	; goto RunTasks

; AddTimer: macro wrapper to AddCoreTimer
; RTOS
; @0 - Task label (address)
; @1 - Timer offset
.macro AddTimer
.if (@1 > MAX_TIMER_MS)
	.error "Timer is too big increase prescaler or reduce timer value"
.endif
				push XL
				push XH
				push ZL
				push ZH
				ldi ZL, low(@0)
				ldi ZH, high(@0)
				ldi XL, low(@1 * TIMER_FACTOR_US/1000) 
				ldi XH, high(@1 * TIMER_FACTOR_US/1000)
				rcall AddCoreTimer

				pop ZH
				pop ZL
				pop XH
				pop XL
.endmacro

; AddCoreTimer: Add task to timer
; RTOS
; Used resources:
; X - time offset (ms)
; Z - task address (in words)
; Y, tmp2, rtmpl, rtmph - temporary (safe)
AddCoreTimer:
				push YL
				push YH
				push rtmp2
				push rtmpl
				push rtmph

				ldi rtmp2, 0xFC ; Start from 0 offset (next line will drop to 0)
PACTMR01:		subi rtmp2, -4
				ldi YL, low(coreTimerQueue)	
				ldi YH, high(coreTimerQueue)
				cpi rtmp2, CORE_TIMER_LENGTH * 4 ; If timerPos == timerQueue+TASK_LENGTH
				brne PACTMR02
				rjmp CoreException	; then sendException 1
PACTMR02:		clr rtmpl
				add YL, rtmp2 ; Add offset to coreTimerQueue
				adc YH, rtmpl
				ld rtmph, Y+ ; Load timer
				ld rtmpl, Y
				cpi rtmpl, 0 ; if low(timer) <> 0x00
				brne PACTMR01 ; then checknext offset
				cpi rtmph, 0 ; if high(timer) <> 0x00
				brne PACTMR01 ; then check next offset
				subi YL, 1 ; Found empty timer. Goto begin of timer record, and
				sbci YH, 0 ; store new task here
				st Y+, XH	; Write timer
				st Y+, XL
				st Y+, ZH	; Write task
				st Y, ZL

				pop rtmph
				pop rtmpl
				pop rtmp2
				pop YH
				pop YL
				ret

; RTOS_TIM0_OVF: Timer0 Overflow interrupt handler
; RTOS
; Used resources:
; Y, rtmp2, rtmpl, rtmph - temporary (safe)
; TODO: Add task to optimize queue by moving last timer to empty position and reduce timers queue.
RTOS_TIM0_OVF:
				push ZL
				push ZH
				push YL
				push YH
				push rtmp2
				push rtmpl
				push rtmph
				in rtmp2, SREG
				push rtmp2

				ldi rtmp2, 0xFC ; Start from begining (next instruction will 0)
PICTMR01:		subi rtmp2, -4
				cpi rtmp2, CORE_TIMER_LENGTH * 4	; If timerPos == timerQueue+TASK_LENGTH
				brne PICTMR02
				rjmp PICTMREND	; then goto return
PICTMR02:		ldi YL, low(coreTimerQueue)
				ldi YH, high(coreTimerQueue)
				clr rtmpl
				add YL, rtmp2
				adc YH, rtmpl
				ld rtmph, Y+
				ld rtmpl, Y+
				cpi rtmpl, 0	; if low(timer) == 0x00
				breq PICTMR03	; then goto check high
				rjmp PICTMR04 ; goto decrement timer
PICTMR03:		cpi rtmph, 0 ; if high(timer) = 0x00
				brne PICTMR04 ; goto decrement timer
				rjmp PICTMR01	; then goto next
PICTMR04:		subi rtmpl, 1 ; dec timerTime
				sbci rtmph, 0
				ld ZH, Y+
				ld ZL, Y+
				sbiw YH:YL, 2
				st -Y, rtmpl	; Store new timer value
				st -Y, rtmph
				cpi rtmpl, 0	; If timer <> 0
				brne PICTMR01	; then goto next
				cpi rtmph, 0
				brne PICTMR01
				rcall AddCoreTask
				rjmp PICTMR01
PICTMREND:
				pop rtmp2
				out SREG, rtmp2
				pop rtmph
				pop rtmpl
				pop rtmp2
				pop YH
				pop YL
				pop ZH
				pop ZL
				;rjmp TIM0_OVF
				reti

; CoreException: Exception handler. Stops program execution and indicate problem.
; RTOS
CoreException:
				cli
				rcall ExceptionHandler ; defined in handlers.asm
				rjmp CoreException
