				ldi ZH, high(SRAM_START) ; Memory Flush
				ldi ZL, low(SRAM_START)
				clr r16
Flush:			st Z+, r16
				cpi ZH, high(RAMEND + 1)
				brne Flush
				cpi ZL, low(RAMEND + 1)
				brne Flush

				ldi ZL, 30 ; Register Flush
				clr ZH
RegFlush:		dec ZL
				st Z, ZH
				brne RegFlush

 				ldi r16, high(RAMEND) ; Stack setup
				ldi r17, low(RAMEND)
				out SPH, r16
				out SPL, r17

; Init Timer0 required by taskman
.if TIM_PRESC == 1
	.equ TIM_PRESC_MASK = 1
.elif TIM_PRESC == 8
	.equ TIM_PRESC_MASK = 2
.elif TIM_PRESC == 64
	.equ TIM_PRESC_MASK = 3
.elif TIM_PRESC == 256
	.equ TIM_PRESC_MASK = 4
.elif TIM_PRESC == 1024
	.equ TIM_PRESC_MASK = 5
.else
	.error "Timer prescaler is wrong"
.endif
				ldi r16, TIM_PRESC_MASK << CS00 ; Set prescaler cycles per count
				store TCCR0B, r16
				ldi r16, 0<<TOV0
				store TIFR0, r16
				ldi r16, 1<<TOIE0 ; Init timer and allo overflow int
				store TIMSK0, r16	
