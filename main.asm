.include "m328pdef.inc" ; Device specific constants (if not found use "rtos/m328pdef.inc"
.include "rtos/macros.inc" ; Device specific macros (load, store, skbc, skbs, clrb, setb for all I/O portstore)

.equ F_CPU = 8000000 ; MCU Frequency
; Low timer prescale gives higher accuracy and can be usefull for alternate use of Timer0 (for ex. PWM)
; However low prescale increases load on system. recommended values: 8 or 64
.equ TIM_PRESC = 64 ; RTOS timer prescaler. 1/8/64/256/1024

.if F_CPU % (TIM_PRESC*256*1000) > (TIM_PRESC*256*1000) / 2 ; Round to closer integer
	.equ TIMER_FACTOR_US = F_CPU/TIM_PRESC/256 + 1
.else
	.equ TIMER_FACTOR_US = F_CPU/TIM_PRESC/256
.endif
.equ MAX_TIMER_MS = (TIMER_FACTOR_US < 1000) ? 65535 : 65535 / TIMER_FACTOR_US


.cseg

; Interrupt vectors (edit to enable required vectors)
.include "vectors.asm"
.org INT_VECTORS_SIZE

; Reset vector
RESET:

; RTOS: Initialisation
.include "rtos/init.asm"
				rjmp InitMain ; jump to application main code

; RTOS: Load Task Manager
.include "rtos/taskman.asm" ; include task manager

; Application interrupt handlers
.include "handlers.asm"

InitMain:
; Application initialisation
.include "init.asm"

; Interrupt on
				sei

; Main: main program code
Main:			
				rcall RunTasks ; Execute task manager (not safe for registers)
; Application main code
.include "appmain.asm"
				rjmp Main

; Tasks executed by task manager
.include "tasks.asm"
