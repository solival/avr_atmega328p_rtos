; Application initialisation code
; Configure ports, timers, and everything else needed for application.

; Set debug led port for output
				setb DDRB, PINB5

; Blink debug led port
				addTask DbgLedOn
