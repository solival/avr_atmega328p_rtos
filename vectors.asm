; Vectors for interrupt handlers.
; By default disabled.
; To enable vector handler remove "reti ;" and implement handler.
; Handler name is predefined after "jmp" for each vector.
; !!! DO NOT IMPLEMENT HANLDERS HERE. USE handlers.asm !!!

.org 0x0000
				jmp RESET ; Reset Handler
.org INT0addr 
 				reti ;jmp EXT_INT0 ; IRQ0 Handler
.org INT1addr
 				reti ;jmp EXT_INT1 ; IRQ1 Handler
.org PCI0addr
				reti ;jmp PCINT0 ; PCINT0 Handler
.org PCI1addr
				reti ;jmp PCINT1 ; PCINT1 Handler
.org PCI2addr
				reti ;jmp PCINT2 ; PCINT2 Handler
.org WDTaddr
				reti ;jmp WDT ; Watchdog Timer Handler
.org OC2Aaddr
				reti ;jmp TIM2_COMPA ; Timer2 Compare A Handler
.org OC2Baddr
				reti ;jmp TIM2_COMPB ; Timer2 Compare B Handler
.org OVF2addr
				reti ;jmp TIM2_OVF ; Timer2 Overflow Handler
.org ICP1addr 
				reti ;jmp TIM1_CAPT ; Timer1 Capture Handler
.org OC1Aaddr 
				reti ;jmp TIM1_COMPA ; Timer1 Compare A Handler
.org OC1Baddr
				reti ;jmp TIM1_COMPB ; Timer1 Compare B Handler
.org OVF1addr
				reti ;jmp TIM1_OVF ; Timer1 Overflow Handler
.org OC0Aaddr
				reti ;jmp TIM0_COMPA ; Timer0 Compare A Handler
.org OC0Baddr
				reti ;jmp TIM0_COMPB ; Timer0 Compare B Handler
.org OVF0addr 
				; Timer 0 overflow is used by RTOS
				; Define TIM0_OVF in handlers.asm to do custom
				; actions for your applications
				jmp RTOS_TIM0_OVF ; Timer0 Overflow Handler
.org SPIaddr
				reti ;jmp SPI_STC ; SPI Transfer Complete Handler
.org URXCaddr
				reti ;jmp USART_RXC ; USART, RX Complete Handler
.org UDREaddr
				reti ;jmp USART_UDRE ; USART, UDR Empty Handler
.org UTXCaddr
				reti ;jmp USART_TXC ; USART, TX Complete Handler
.org ADCCaddr
				reti ;jmp ADC ; ADC Conversion Complete Handler
.org ERDYaddr
				reti ;jmp EE_RDY ; EEPROM Ready Handler
.org ACIaddr
				reti ;jmp ANA_COMP ; Analog Comparator Handler
.org TWIaddr
				reti ;jmp TWI ; 2-wire Serial Interface Handler
.org SPMRaddr
				reti ;jmp SPM_RDY ; Store Program Memory Ready Handler
