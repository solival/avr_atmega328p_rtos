; Application interrupt handlers
; Enable interrupt in vectors.asm
; And implement handler in this file

; TIM0_OVF: Timer0 overflow handler
; Used by RTOS
; Can also be used here, by uncomment 'rjmp TIM0_OVF' in rtos/taskman.asm
; Return only by 'reti'
;TIM0_OVF:
;				reti

; ExceptionHandler: Handler of program exception thrown by RTOS
; Put your indicating of problem here. 
; Interrupts disabled.
; Must end by "ret"
ExceptionHandler:
				ret
