; Application tasks for timer/task manager

; DbgLedOn: Switch degbug led on and schedule DbgLedOff in 200 ms
; Used resources:
; PIND5 of PORTD - led
DbgLedOn:
				setb PORTD, PIND5
				addTimer DbgLedOff, 200
				ret

; DbgLedOff: Switch debug led off and schedule DbgLedOn in 200ms
DbgLedOff:
				clrb PORTD, PIND5
				addTimer DbgLedOn, 200
				ret
